#include <iostream>
#include <string>
#include <vector>

#include "Rectangle.h"
#include "Square.h"
#include "Circle.h"

using namespace std;

int main()
{
	// Create shapes
	Shape* shapes[3];
	shapes[0] = new Rectangle(10.0f, 5.0f);
	shapes[1] = new Square(4.0f);
	shapes[2] = new Circle(2.5f);

	for (int i = 0; i < 3; i++)
	{
		Shape* shape = shapes[i];
		cout << shape->type() << "		Perimeter: " << shape->perimeter() << "	Area: " << shape->area() << endl;
	}

	// Delete allocated memory
	for (int i = 0; i < 3; i++)
	{
		delete shapes[i];
	}

	system("pause");
	return 0;
}