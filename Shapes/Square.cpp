#include "Square.h"



Square::Square(float length)
{
	mLength = length;
}

Square::~Square()
{
}

string Square::type()
{
	return "Square";
}

float Square::perimeter()
{
	return 4.0f * mLength;
}

float Square::area()
{
	return mLength * mLength;
}
