#include "Rectangle.h"




Rectangle::Rectangle(float length, float width)
{
	mLength = length;
	mWidth = width;
}

Rectangle::~Rectangle()
{
}

string Rectangle::type()
{
	return "Rectangle";
}

float Rectangle::perimeter()
{
	return 2.0f * (mLength + mWidth);
}

float Rectangle::area()
{
	return mLength * mWidth;
}
