#pragma once
#include "Shape.h"
class Rectangle :
	public Shape
{
public:
	Rectangle(float length, float width);
	~Rectangle();

	string type();
	float perimeter();
	float area();

private:
	float mLength;
	float mWidth;
};

