// ChessBoard.cpp : Defines the entry point for the console application.
//

#include <string>
#include <iostream>
#include <conio.h>

using namespace std;

void displayTree(int levels)
{
	for (int i = 0; i < levels; i++)
	{
		for (int j = 0; j <= i; j++)
		{
			cout << "*";
		}
		cout << endl;
	}
}

void displayChessBoard()
{
	// Define constants. This makes configuration easier.
	const int rows = 6;
	const int columns = 8;
	const string black = "X";
	const string white = "O";

	for (int row = 0; row < rows; row++)
	{
		for (int column = 0; column < columns; column++)
		{
			// Determine tile color
			bool isBlack = true;
			if (row % 2 == 0)
			{
				isBlack = column % 2 != 0;
			}
			else
			{
				isBlack = column % 2 == 0;
			}

			// Show tile
			if (isBlack)
			{
				cout << black;
			}
			else
			{
				cout << white;
			}
			cout << " ";
		}
		cout << endl;
	}
}

int main()
{
	//displayTree(8);
	displayChessBoard();

	system("pause");
	return 0;
}
