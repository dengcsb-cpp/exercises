#include <iostream>
#include <string>

using namespace std;

// range is used to modify the range of the randomized number (rand() % range)
// minValue is the offset applied to the randomized number to a minimum value. This defaults to 0 which means there's no offset.
void fillArray(int numbers[], int size, int range = 0, int minValue = 0);
int getLargestNumber(int numbers[], int size);
void printNumbers(int numbers[], int size);

int main()
{
	// Create the array
	int numbers[10];
	fillArray(numbers, 10, 100, 1);

	// Print the array
	printNumbers(numbers, 10);

	// Print the largest number
	int largest = getLargestNumber(numbers, 10);
	cout << "Largest number: " + largest << endl;

	system("pause");
	return 0;
}

void fillArray(int numbers[], int size, int range = 0, int minValue = 0)
{
	if (range < 0) throw new exception("Range cannot be lower than 1");

	for (int i = 0; i < size; i++)
	{
		// Check if default 'range' parameter is set and use the range specified
		if (range > 0) numbers[i] = rand() % range + minValue;
		else numbers[i] = rand() + minValue;
	}
}

int getLargestNumber(int numbers[], int size)
{
	// Get largest
	int largest = numbers[0];
	for (int i = 1; i < size - 1; i++)
	{
		if (numbers[i] > largest)
		{
			largest = numbers[i];
		}
	}

	return largest;
}

void printNumbers(int numbers[], int size)
{
	for (int i = 0; i < size; i++)
	{
		cout << numbers[i] << endl;
	}
}
