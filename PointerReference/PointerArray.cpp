#include <iostream>
#include <string>
#include <cstdlib>
#include <ctime>

using namespace std;

void fillArray(int* items, int size);
int* createRandomArray(int size, int range);
void printArray(int* items, int size);

int main()
{
	srand(time(0));

	// Create an array in main and let FillArray initialize its values
	int* numbers1 = new int[10];
	fillArray(numbers1, 10);
	printArray(numbers1, 10);
	delete[] numbers1; // Don't forget to delete allocated memory once you're done with it.

	// Let the function create the array for you.
	int* numbers2 = createRandomArray(10, 100);
	printArray(numbers2, 10);
	delete[] numbers2; // Even if 'main' didn't allocate the memory, 'main' is now responsible since it was returned from a function called in this scope.

	system("pause");
	return 0;
}

// Given the pointer to the array, we have access to all of its elements using pointer arithmetic (bracket operator). Remember that we are only passing 4 bytes no matter how large the array is.
void fillArray(int* items, int size)
{
	for (int i=0; i<size; i++)
	{
		items[i] = rand() % 10;
	}
}

// Create an array filled with random values and return the pointer/address to that allocated memory.
int* createRandomArray(int size, int range)
{
	int* items = new int[size];

	for (int i=0; i<size; i++)
	{
		items[i] = rand() % range;
	}

	// It's impossible to delete it here because we have no way of knowing the lifecycle (when to delete) of this array.
	return items;
}

// Since array is just a pointer to the first element (index 0), we can simply pass the pointer (*)
void printArray(int* items, int size)
{
	for (int i = 0; i<size; i++)
	{
		cout << items[i] << endl;
	}
}