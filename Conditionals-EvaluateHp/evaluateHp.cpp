#include <iostream>
#include <string>

using namespace std;

int main()
{
	// Get input for max HP
	int maxHp;
	cout << "Input max HP: ";
	cin >> maxHp;

	// Get input for current HP
	int currentHp;
	cout << "Input current HP: ";
	cin >> currentHp;

	// Check if input is valid
	if (currentHp > maxHp)
	{
		cout << "Current HP is higher than Max HP;";
		return 0;
	}

	// Normalize HP to 0..1
	float normalized = (float)currentHp / (float)maxHp;

	if (normalized == 1.0f)
	{
		cout << "Full" << endl;
	}
	else if (normalized >= 0.5f)
	{
		cout << "Green" << endl;
	}
	else if (normalized >= 0.2f)
	{
		cout << "Yellow" << endl;
	}
	else if (normalized > 0)
	{
		cout << "Red" << endl;
	}
	else
	{
		cout << "Dead" << endl;
	}

	while (true)
	{
		cout << "Input 'x' to break out of loop";
		char test;
		cin >> test;

		if (test == 'x') break; 
	}



	system("pause");
	return 0;
}