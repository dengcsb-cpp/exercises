#include <iostream>
#include <string>

using namespace std;

void sumAverage()
{
	// Get the number of values to be evaluated
	int n;
	cout << "Input number of values to be computed: ";
	cin >> n;

	// Compute sum based on 'n'
	int sum = 0;
	for (int i = 0; i < n; i++)
	{
		// Get input for each value
		int x;
		cout << "Input value " << i + 1 << ": ";
		cin >> x;

		// Add x to sum
		sum += x;
	}

	float average = (float)sum / n;

	cout << endl;
	cout << "Sum: " << sum << endl;
	cout << "Average: " << average << endl;
}

void factorial()
{
	// Get input. Loop until valid
	int value = -1;
	while (value < 0)
	{
		cout << "Input value: ";
		cin >> value;
	}

	// Compute factorial
	int n = value;
	int factorial = 1;
	while (n > 0)
	{
		factorial *= n;
		n--;
	}

	// Output
	cout << value << "! = " << factorial << endl;
}

void arithmeticProgression()
{
	// Get input for starting value
	int start;
	cout << "Input starting value: ";
	cin >> start;

	// Get input for number of progression
	int progression = -1;
	while (progression < 0)
	{
		cout << "Input number of progression/terms: ";
		cin >> progression;
	}

	// Compute arithmetic progression
	int result = 0;
	for (int i = start; i < start + progression; i++)
	{
		result += i;
	}

	// Output result
	cout << "Result: " << result << endl;
}

void oddEven()
{
	// Get input for number of terms
	int numTerms = -1;
	while (numTerms < 0)
	{
		cout << "Input number of terms: ";
		cin >> numTerms;
	}

	// Get input for odd/even
	int isEven = -1;
	while (isEven != 0 && isEven != 1)
	{
		cout << "Even/Odd [0/1]?: ";
		cin >> isEven;
	}

	// Print odd/even
	int numSuccess = 0;
	for (int i = 0; numSuccess < numTerms; i++)
	{
		if (isEven == 0)
		{
			if (i % 2 == 0)
			{
				numSuccess++;
				cout << i << endl;
			}
		}
		else
		{
			if (i % 2 == 1)
			{
				numSuccess++;
				cout << i << endl;
			}
		}
	}
}

int main()
{
 
	system("pause");
	return 0;
}