#include <iostream>
#include <string>
#include "Wizard.h"

using namespace std;

int main()
{
	Wizard* wizard1 = new Wizard();
	//wizard1->viewStats();
	
	// Input name
	string name;
	cout << "Input name: ";
	cin >> name;

	// Input HP
	int hp;
	cout << "Input HP: ";
	cin >> hp;

	// Input MP
	int mp;
	cout << "Input MP: ";
	cin >> mp;

	// Input XP
	int xp;
	cout << "Input XP: ";
	cin >> xp;

	Spell* thunderBolt = new Spell("Thunderbolt", 15, 15);
	Wizard* wizard2 = new Wizard(name, hp, mp, thunderBolt);
	//wizard2->viewStats();
	cout << wizard2->getHp() << endl;

	while (wizard1->getHp() > 0 || wizard2->getHp() > 0 || wizard1->getMp() > 0 || wizard2->getMp() > 0)
	{
		wizard1->castSpell(wizard2);
		wizard2->castSpell(wizard1);
	}


	system("pause");
	delete wizard1;
	delete wizard2;
	return 0;
}