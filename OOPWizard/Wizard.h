#include <string>
#include <iostream>
#include "Spell.h"

using namespace std;

class Spell;

#pragma once
class Wizard
{
public:
	// Constructor and destructor
	// Set default values for member variables
	Wizard();
	Wizard(string name, int hp, int mp, Spell* spell);
	~Wizard();

	bool castSpell(Wizard* target);
	void viewStats();

	// Getter
	int getHp();
	int getMp();
	string getName();
	Spell* getSpell();

	void takeDamage(int damage);

private:
	string mName;
	int mHitPoints;
	int mMagicPoints;
	Spell* mSpell;
};

