#pragma once
#include <iostream>

using namespace std;

enum StatType
{
	StatPower,
	StatVitality,
	StatDexterity,
	StatAgility
};

struct Stats
{
	int hp = 0;
	int power = 0;
	int vitality = 0;
	int agility = 0;
	int dexterity = 0;
};

void addStats(Stats& source, const Stats& toAdd);
void printStats(const Stats& stats);