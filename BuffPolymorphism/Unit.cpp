#include "Unit.h"

Unit::Unit(string name, const Stats & stats)
{
	mName = name;
	mStats = stats;

	// To test heal
	mCurrentHp = 1;
}

Unit::~Unit()
{
	// Delete all skills
	for (int i = 0; i < mSkills.size(); i++)
	{
		delete mSkills[i];
	}
	mSkills.clear();
}

string Unit::getName()
{
	return mName;
}

Stats & Unit::getStats()
{
	return mStats;
}

int Unit::getCurrentHp()
{
	return mCurrentHp;
}

void Unit::heal(int amount)
{
	if (amount < 1) throw new exception("Cannot heal for an amount lower than 1");
	mCurrentHp += amount;

	// Clamp hp to max hp
	if (mCurrentHp > mStats.hp) mCurrentHp = mStats.hp;
}

const vector<Skill*>& Unit::getSkills()
{
	return mSkills;
}

void Unit::addSkill(Skill * skill)
{
	// Set this skill's actor to self
	skill->setActor(this);

	// You may want to check if the skill already exists to prevent duplicates. This depends on how much restriction you need.
	mSkills.push_back(skill);
}

bool Unit::alive()
{
	return mCurrentHp > 0;
}

void Unit::displayStatus()
{
	// Name
	cout << "Name: " << mName << endl;
	// HP
	cout << "HP: " << mCurrentHp << "/" << mStats.hp << endl;
	// Stats
	cout << "Pow: " << mStats.power << endl;
	cout << "Vit: " << mStats.vitality << endl;
	cout << "Dex: " << mStats.dexterity << endl;
	cout << "Agi: " << mStats.agility << endl;
}
