#include <iostream>
#include <string>

using namespace std;

struct Node
{
	int data = 0;
	Node* next;
};

Node* createLinkedList();
Node* getNodeAtIndex(Node* head, int index);
void printLinkedList(Node* head);
Node* removeItemAtIndex(Node* head, int index);
void pushBack(Node* head, int data);

int main()
{
	// Create the linked list and store a pointer to it
	Node* head = createLinkedList();

	// Print the linked list before we do something
	printLinkedList(head);
	cout << endl;

	// Let's try to get the node at index 2 and print its value
	Node* nodeAtIndex2 = getNodeAtIndex(head, 2);
	cout << "The value at index 2 of the linked list: " << nodeAtIndex2->data << endl << endl;

	// Let's try to delete the node at index 2. Remember that we need to reassign the head pointer based on the function's result since it might change (eg. passing index 0)
	head = removeItemAtIndex(head, 2);

	// Let's try printing the list again. There should only be 4 nodes left
	printLinkedList(head);
	cout << endl;

	// The program has ended. Remember to always delete memory we allocated. We still have 4 nodes to delete. The idea is to use the same iteration logic of linked list (which is keep on going until pointer points to NULL)
	while (head != NULL)
	{
		// Store it in a temporary variable so we can delete it before changing the address of "head"
		Node* temp = head;
		head = head->next;
		delete temp;
	}

	system("pause");
	return 0;
}

Node * createLinkedList()
{
	Node* head = NULL;
	Node* current = NULL;
	Node* previous = NULL;

	current = new Node;
	current->data = 10;
	previous = current;
	head = current;

	current = new Node;
	current->data = 20;
	previous->next = current;
	previous = current;

	current = new Node;
	current->data = 30;
	previous->next = current;
	previous = current;

	current = new Node;
	current->data = 40;
	previous->next = current;
	previous = current;

	current = new Node;
	current->data = 50;
	previous->next = current;
	current->next = NULL;

	return head;
}

Node * getNodeAtIndex(Node * head, int index)
{
	for (int i = 0; i < index; i++)
	{
		head = head->next;
	}

	return head;
}

void printLinkedList(Node * head)
{
	// Loop until the current pointer points to NULL (end)
	while (head != NULL)
	{
		cout << head->data << endl;
		// Switch current pointer to the next node
		head = head->next;
	}
}

int sizeOfList(Node* head)
{
	// This line is actually optional. You can use the "head" parameter directly. This won't affect the caller since this function is given a "copy" of the memory address of the head node.
	Node* current = head; 

	// Keep incrementing 'size' until we reach the last node.
	int size = 0;
	while (current != NULL)
	{
		size++;
		current = current->next;
	}
	return size;
}

Node* removeItemAtIndex(Node * head, int index)
{
	// Store previous node so we can reconnect it to the tail of the deleted node.
	Node* previous = NULL;

	// To make this a lot simpler. If the index is zero (meaning we want to delete the head node), we handle it right away and remove the head node.
	if (index == 0)
	{
		previous = head;
		head = head->next;

		delete previous;

		// Here, the head node changed since we removed the "old" head node. What we're returning here was the node next to the "original" head node.
		return head;
	}


	// To make it readable and more intuitive for you, I used a separate variable to store the pointer of the node at the index we need to delete. Initially, toDelete is set to the head node
	Node* toDelete = head;

	// Iterate until we get to the node we want to delete
	for (int i = 0; i < index; i++)
	{
		previous = toDelete;
		toDelete = toDelete->next;
	}

	// Now that we found the node to delete, we can reconnect the link using the information of the previous and next nodes
	// Step 1. Set the "next" pointer of the previous node to the "next" pointer of the toDelete node. This will remove toDelete node from the linked list since we are saying that the previous node already connects to the node next to toDelete.
	previous->next = toDelete->next;
	// Step 2. Delete toDelete node from our memory pool so we can reclaim the used memory
	delete toDelete;

	return head;
}

void pushBack(Node* head, int data)
{
	Node* current = head;

	// Get the last node since we'll connect its tail to the new node later.
	Node* lastNode = NULL;
	while (current != NULL)
	{
		lastNode = current;
		current = current->next;
	}

	// Create the new node and connect the lastNode's tail to this node. Remember that this node's tail should point to NULL since this is now the last node.
	Node* newNode = new Node;
	newNode->data = data;
	newNode->next = NULL;
	lastNode->next = newNode;
}
