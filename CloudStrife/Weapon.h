#pragma once
#include "Materia.h"
#include <vector>
#include <string>

class Weapon
{
public:
	Weapon(std::string name, int atk);
	~Weapon();

	std::string getName();
	int getAtk();

	void insertMateria(Materia* materia);
	Materia* removeMateria(int index);
	std::vector<Materia*> clearAllMateria();

private:
	std::string mName;
	int mAtk;
	std::vector<Materia*> mMaterias;
};

