#pragma once
#include "Weapon.h"
#include "Armor.h"
#include "Accessory.h"
#include "Skill.h"
#include "HealingItem.h"
#include <vector>
#include <string>

class HealingItem;
class Skill;

class Unit
{
public:
	Unit(std::string name);
	~Unit();

	// Basic
	std::string getName();
	int getLevel();
	int getHp();
	int getHpMax();
	int getMp();
	int getMpMax();
	int getCurrentExp();
	void addExp(int exp);
	int getNextExp();

	void addHp(int amount);
	void takeDamage(int damage);
	void heal(int amount);

	// Equipment
	Weapon* getWeapon();
	Weapon* equipWeapon(Weapon* weapon);
	Armor* getArmor();
	Armor* equipArmor(Armor* armor);
	Accessory* getAccessory();
	Accessory* equipAccessory(Accessory* accessory);

	// Items
	void addItem(Item* item);
	void useItem(Item* item);
	void printInventory();

	// Skills
	void useSkill(Skill* skill);

	// Primary Stats
	int getStrength();
	int getDexterity();
	int getVitality();
	int getMagic();
	int getSpirit();
	int getLuck();

	// Derived Stats
	int getAttack();
	float getAttackPercent();
	int getDefense();
	float getDefensePercent();
	int getMagicAtk();
	int getMagicDef();
	float getMagicDefPercent();

private:
	// Basic
	std::string mName;
	int mLevel;
	int mHp;
	int mHpMax;
	int mMp;
	int mMpMax;
	int mCurrentExp;
	int mNextExp;

	// Equipment
	Weapon* mWeapon;
	Armor* mArmor;
	Accessory* mAccessory;

	// Items
	std::vector<Item*> mItems;

	// Skills
	std::vector<Skill*> mSkills;

	// Primary Stats
	int mStrength;
	int mDexterity;
	int mVitality;
	int mMagic;
	int mSpirit;
	int mLuck;

	// Derived stats
	int mAttack;
	float mAttackPercent;
	int mDefense;
	float mDefensePercent;
	int mMagicAtk;
	int mMagicDef;
	float mMagicDefPercent;

	void levelUp();
};

