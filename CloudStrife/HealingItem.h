#pragma once
#include "Unit.h"
#include "Item.h"
#include <string>

class Unit;

class HealingItem : Item
{
public:
	HealingItem(std::string name, int healAmount);
	~HealingItem();

	int getHealAmount();

private:
	int mHealAmount;
};
