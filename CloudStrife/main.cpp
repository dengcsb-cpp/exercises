#include <iostream>
#include <string>

#include "Unit.h"

using namespace std;

int main()
{
	Unit* cloud = new Unit("Cloud Strife");

	cout << "Adding items to inventory..." << endl;
	system("pause");
	system("cls");
	HealingItem* redPotion = new HealingItem("Red Potion", 100);
	HealingItem* orangePotion = new HealingItem("Orange Potion", 250);
	cloud->addItem(redPotion);
	cloud->addItem(orangePotion);
	
	cout << endl << "Displaying inventory..." << endl;
	system("pause");
	system("cls");
	cloud->printInventory();

	cout << endl << "Simulating damage...";
	system("pause");
	system("cls");
	cout << cloud->getName() << " HP before taking damage: " << cloud->getHp() << endl;
	cloud->takeDamage(200);
	cout << cloud->getName() << " HP after taking 200 damage: " << cloud->getHp() << endl;

	cout << endl << "Using Red Potion...";
	system("pause");
	system("cls");
	cloud->useItem(redPotion);
	cout << cloud->getName() << " HP after using Red Potion: " << cloud->getHp() << endl;

	system("pause");
	return 0;
}