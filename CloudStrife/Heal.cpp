#include "Heal.h"




Heal::Heal(int healPercent)
{
	mHealPercent = healPercent;
}

Heal::~Heal()
{
}

bool Heal::activate(Unit* actor, Unit* target)
{
	// Check if parent class will allow the skill to activate
	if (Skill::activate(actor, target))
	{
		// Heal 30% of target's max HP
		int amount = (float)target->getHpMax * mHealPercent;
		target->heal(amount);
		cout << target->getName() << " is healed for " << amount << " HP" << endl;
	}
}
