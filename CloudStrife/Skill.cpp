#include "Skill.h"





Skill::Skill(std::string name, int mpCost)
{
	mName = name;
	mMpCost = mpCost;
}

Skill::~Skill()
{
}

std::string Skill::getName()
{
	return mName;
}

bool Skill::activate(Unit* actor, Unit * target)
{
	if (actor->getMp() < mMpCost) return false;

	cout << mName << " is casted on " << target->getName() << endl;
	return true;
}
