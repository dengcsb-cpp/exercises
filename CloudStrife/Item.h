#pragma once
#include "Unit.h"

class Unit;

class Item
{
public:
	Item(std::string name);
	~Item();

	std::string getName();
	void use(Unit* target);
	void printDescription();

private:
	std::string mName;
};

