#include "Weapon.h"




Weapon::Weapon(std::string name, int atk)
{
	mName = name;
	mAtk = atk;
}

Weapon::~Weapon()
{
}

std::string Weapon::getName()
{
	return mName;
}

int Weapon::getAtk()
{
	return mAtk;
}

void Weapon::insertMateria(Materia * materia)
{
	mMaterias.push_back(materia);
}

Materia * Weapon::removeMateria(int index)
{
	Materia* materia = mMaterias[index];
	mMaterias.erase(mMaterias.begin() + 1);

	return materia;
}

std::vector<Materia*> Weapon::clearAllMateria()
{
	std::vector<Materia*> unequipped = mMaterias;
	mMaterias.clear();
	return unequipped;
}
