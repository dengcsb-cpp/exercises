#include "Unit.h"
#include <iostream>

using namespace std;

Unit::Unit(std::string name)
{
	mName = name;

	// Start with 1000 HP
	mHp = 1000;
	mHpMax = 1000;
}

Unit::~Unit()
{
}

std::string Unit::getName()
{
	return mName;
}

int Unit::getLevel()
{
	return mLevel;
}

int Unit::getHp()
{
	return mHp;
}

int Unit::getHpMax()
{
	return mHpMax;
}

int Unit::getMp()
{
	return mMp;
}

int Unit::getMpMax()
{
	return mMpMax;
}

int Unit::getCurrentExp()
{
	return mCurrentExp;
}

void Unit::addExp(int exp)
{
	mCurrentExp += exp;

	// If current XP reached next XP, level up
	if (mCurrentExp > getNextExp())
	{
		levelUp();
	}
}

int Unit::getNextExp()
{
	// For every level, increase required xp by 10
	return mLevel * 10;
}

Weapon * Unit::getWeapon()
{
	return mWeapon;
}

Weapon* Unit::equipWeapon(Weapon * weapon)
{
	Weapon* old = mWeapon;
	mWeapon = weapon;
	return old;
}

Armor * Unit::getArmor()
{
	return mArmor;
}

Armor * Unit::equipArmor(Armor * armor)
{
	Armor* old = mArmor;
	mArmor = armor;
	return old;
}

Accessory * Unit::getAccessory()
{
	return mAccessory;
}

Accessory* Unit::equipAccessory(Accessory * accessory)
{
	Accessory* old = mAccessory;
	mAccessory = accessory;
	return old;
}

void Unit::addItem(Item * item)
{
	mItems.push_back(item);
	cout << "You got " << item->getName() << endl;
}

void Unit::useItem(Item * item)
{
	// Use the item
	item->use(this);

	// Search for the item and delete it
	for (int i = 0; i < mItems.size(); i++)
	{
		if (item == mItems[i])
		{
			Item* itemToDelete = mItems[i];
			mItems.erase(mItems.begin() + i);
			delete itemToDelete;

			break;
		}
	}
}

void Unit::printInventory()
{
	for (int i = 0; i < mItems.size(); i++)
	{
		mItems[i]->printDescription();
	}
}

void Unit::useSkill(Skill * skill)
{
	cout << getName() << " is using " << skill->getName() << endl;
}

int Unit::getStrength()
{
	return mStrength;
}

int Unit::getDexterity()
{
	return mDexterity;
}

int Unit::getVitality()
{
	return mVitality;
}

int Unit::getMagic()
{
	return mMagic;
}

int Unit::getSpirit()
{
	return mSpirit;
}

int Unit::getLuck()
{
	return mLuck;
}

int Unit::getAttack()
{
	return mAttack;
}

float Unit::getAttackPercent()
{
	// This can be derived (computed) instead of directly returning the private variable
	return mAttackPercent;
}

int Unit::getDefense()
{
	// This can be derived (computed) instead of directly returning the private variable
	return mDefense;
}

float Unit::getDefensePercent()
{
	// This can be derived (computed) instead of directly returning the private variable
	return mDefensePercent;
}

int Unit::getMagicAtk()
{
	// This can be derived (computed) instead of directly returning the private variable
	return mMagicAtk;
}

int Unit::getMagicDef()
{
	// This can be derived (computed) instead of directly returning the private variable
	return mMagicDef;
}

float Unit::getMagicDefPercent()
{
	// This can be derived (computed) instead of directly returning the private variable
	return mMagicDefPercent;
}

void Unit::addHp(int amount)
{
	mHp += amount;
	if (mHp > mHpMax) mHp = mHpMax;
}

void Unit::takeDamage(int damage)
{
	mHp -= damage;
	if (mHp < 0) mHp = 0;
}

void Unit::heal(int amount)
{
	mHp += amount;
	if (mHp > mHpMax) mHp = mHpMax;
}

void Unit::levelUp()
{
	mCurrentExp = 0;
	mLevel++;

	// Increase stats here
}
