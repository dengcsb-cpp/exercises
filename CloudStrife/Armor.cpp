#include "Armor.h"




Armor::Armor(std::string name, int def)
{
	mName = name;
	mDef = def;
}

Armor::~Armor()
{
}

std::string Armor::getName()
{
	return mName;
}

int Armor::getDef()
{
	return mDef;
}

void Armor::insertMateria(Materia * materia)
{
	mMaterias.push_back(materia);
}

Materia * Armor::removeMateria(int index)
{
	Materia* materia = mMaterias[index];
	mMaterias.erase(mMaterias.begin() + index);

	return materia;
}

void Armor::clearAllMateria()
{
	mMaterias.clear();
}
