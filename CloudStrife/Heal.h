#pragma once
#include "Skill.h"
class Heal :
	public Skill
{
public:
	Heal(int healPercent);
	~Heal();

	bool activate(Unit* actor, Unit* target);

private:
	float mHealPercent;
};

