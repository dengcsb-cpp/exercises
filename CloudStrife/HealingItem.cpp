#include "HealingItem.h"


HealingItem::HealingItem(std::string name, int healAmount)
	: Item(name)
{
	mHealAmount = healAmount;
}

HealingItem::~HealingItem()
{
}

int HealingItem::getHealAmount()
{
	return mHealAmount;
}
