#include <iostream>
#include <string>

using namespace std;

int main()
{
	// Initialize variables
	string firstName;
	string lastName;
	int age;
	float money;
	bool hasEatenBreakfast;

	// Get user input
	cout << "What is your first name? ";
	cin >> firstName;
	cout << "What is your last name? ";
	cin >> lastName;
	cout << "How old are you? ";
	cin >> age;
	cout << "How much do you have in you (money)? ";
	cin >> money;
	cout << "Have you eaten breakfast today? ";
	cin >> hasEatenBreakfast;

	// Clear screen
	system("cls");

	// Output result
	cout << "Name:\t" << lastName << ", " << firstName << endl;
	cout << "Age:\t" << age << endl;
	cout << "Money:\t" << money;
	cout << "Has eaten breakfast:\t" << hasEatenBreakfast << endl;

	system("pause");
	return 0;
}