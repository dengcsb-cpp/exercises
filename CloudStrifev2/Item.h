#pragma once
#include <string>
#include "Unit.h"

using namespace std;

class Item
{
public:
	Item(string name);
	~Item();

	string getName();
	void use(Unit* target);
	void printDescription();

private:
	string mName;
};

