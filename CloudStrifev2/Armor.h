#pragma once
#include <string>
#include <vector>
#include "Materia.h"

using namespace std;

class Armor
{
public:
	Armor();
	~Armor();

	string getName();
	int getDef();

	const vector<Materia*>& getMaterias();
	void insertMateria(Materia* materia);
	void removeMateria(Materia* materia);
	vector<Materia*> clearAllMaterias();

private:
	string mName;
	int mDef;
	int mGold;
	vector<Materia*> mMaterias;
};

