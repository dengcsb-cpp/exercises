#pragma once
#include <string>
class Materia
{
public:
	Materia();
	~Materia();

	std::string getName();

private:
	std::string mName;
};

