#pragma once
#include <string>
#include "Unit.h"

using namespace std;

class Skill
{
public:
	Skill(string name, int mpCost);
	~Skill();

	string getName();
	bool activate(Unit* actor, Unit* target);

private:
	string mName;
	int mMpCost;
};

