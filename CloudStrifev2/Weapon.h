#pragma once
#include <string>
#include <vector>
#include "Materia.h"

using namespace std;

class Weapon
{
public:
	Weapon();
	~Weapon();

	string getName();
	int getAtk();

	const vector<Materia*>& getMaterias();
	void insertMateria(Materia* materia);
	void removeMateria(Materia* materia);
	vector<Materia*> clearAllMaterias();

private:
	string mName;
	int mAtk;
	int mGold;
	vector<Materia*> mMaterias;
};

