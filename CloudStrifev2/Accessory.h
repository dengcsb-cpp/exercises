#pragma once
#include <string>

using namespace std;

class Accessory
{
public:
	Accessory();
	~Accessory();

	string getName();

private:
	string mName;
	int mGold;
};

