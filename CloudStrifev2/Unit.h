#pragma once
#include <string>
#include "Weapon.h"
#include "Armor.h"
#include "Accessory.h"
#include "Skill.h"
#include "Item.h"
using namespace std;

class Unit
{
public:
	Unit();
	~Unit();

	string getName();
	int getLevel();
	int getHp();
	int getMaxHp();
	int getMp();
	int getMaxMp();
	int getCurrentXp();

	void addXp(int xp);
	void heal(int hp);
	void takeDamage(int damage);
	
	// Equipment
	Weapon* getWeapon();
	Weapon* equipWeapon(Weapon* weapon);
	Armor* getArmor();
	Armor* equipArmor(Armor* armor);
	Accessory* getAccessory();
	Accessory* equipAccessory(Accessory* accessory);

	// Items
	void addItem(Item* item);
	void useItem(Item* item);
	void printInventory();

	// Skill
	void useSkill(Skill* skill, Unit* target);

	// Primary Stats
	int getStrength();
	int getDexterity();
	int getVitality();
	int getMagic();
	int getSpirit();
	int getLuck();

	// Derived stats
	int getAttack();
	float getAttackPercent();
	int getDefense();
	float getDefensePercent();
	int getMagicAtk();
	int getMagicDef();
	float getMagicDefPercent();

private:
	string mName;
	int mLevel;
	int mHp;
	int mMp;
	int mCurrentXp;
	int mNextXp;

	// Primary Stats
	int mStrength;
	int mDexterity;
	int mVitality;
	int mMagic;
	int mSpirit;
	int mLuck;

	// Equipment
	Weapon* mWeapon;
	Armor* mArmor;
	Accessory* mAccessory;

	// Items
	vector<Item*> mItems;

	// Skills
	vector<Skill*> mSkills;

	void levelUp();
};

