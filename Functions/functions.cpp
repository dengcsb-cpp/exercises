#include <iostream>
#include <string>

using namespace std;

float area(float radius);
int sum(int x, int y);
int factorial(int x);

float area(float radius)
{
	float area = radius * radius * 3.14f;
	return area;
}

int sum(int x, int y)
{
	return x + y;
}

int factorial(int x)
{
	//if (x < 0) throw new exception("Factorial: input must not be lower than 0");
	if (x < 0) return 0;

	int factorial = 1;
	while (x > 0)
	{
		factorial *= x;
		x--;
	}

	return factorial;
}

int main()
{
	int input = 0;
	cout << input << "! = " << factorial(-1) << endl;

	system("pause");
	return 0;
}