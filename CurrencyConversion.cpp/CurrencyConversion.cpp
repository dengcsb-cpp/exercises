#include <iostream>
#include <string>

using namespace std;

int main()
{
	const float EXCHANGE_RATE = 47.74f;

	// Get input
	float usd;
	cout << "Input USD: ";
	cin >> usd;

	// Compute
	float php = usd * EXCHANGE_RATE;

	// Output result
	cout << "PHP: " << php << endl;

	system("pause");
	return 0;
}