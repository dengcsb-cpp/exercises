#include <iostream>
#include <string>
#include <cstdlib>
#include <ctime>
#include <vector>

using namespace std;

struct Roll
{
	string type;
	int rank;
	int value;
	int dice[3];
};

void playRound();
Roll* rollDice();
void printRoll(Roll * roll);
void playRound();

int main()
{
	// Money should be declared here. With reference, we can pass this around.
	int money = 90000;

	for (int i = 0; i < 10; i++)
	{
		playRound();
	}

	system("pause");
	return 0;
}

// Create dynamically allocated Roll struct. Remember that only the 'memory address' is being returned since this is a pointer.
Roll* rollDice()
{
	Roll* roll = new Roll;

	for (int i = 0; i < 3; i++)
	{
		roll->dice[i] = rand() % 6 + 1;
	}

	return roll;
}

// With pointer, only the memory address is being passed (4 bytes). Inside the function, we dereference this pointer so that we can access the actual struct. This is very efficient since we're always passing 4 bytes no matter how big the data structure is.
void printRoll(Roll * roll)
{
	for (int i = 0; i < 3; i++)
	{
		cout << roll->dice[i] << " ";
	}
	cout << endl;
}

void playRound()
{
	// Declare pointer outside of the loop so that we can access it from this scope (outside the loop).
	Roll* roll;
	for (int i = 0; i < 3; i++)
	{
		// Request a dynamically allocated struct using 'rollDice'. Remember that we can store the memory address of the new memory by using a pointer.
		roll = rollDice();
		printRoll(roll);

		// If it's a non bust, exit out of the loop.
		if (roll->type != "Bust")
		{
			break;
		}

		// We are required to null out the 'roll' pointer since it's possible that we're storing address to deleted memory
		delete roll;
		roll = NULL;
	}

	// If the roll is not NULL, we delete it. Do not ever call delete on pointer which points to deleted memory (crashes).
	if (roll != NULL) delete roll;
}