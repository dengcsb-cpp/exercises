#include <iostream>
#include <string>

using namespace std;

int main()
{
	// Get input for x y
	int x;
	int y;
	cout << "Input x: ";
	cin >> x;
	cout << "Input y: ";
	cin >> y;

	// Relational expressions
	cout << x << " == " << y << " = " << (x==y) << endl;
	cout << x << " != " << y << " = " << (x!=y) << endl;
	cout << x << " > " << y << " = " << (x>y) << endl;
	cout << x << " < " << y << " = " << (x<y) << endl;
	cout << x << " >= " << y << " = " << (x>=y) << endl;
	cout << x << " <= " << y << " = " << (x<=y) << endl;

	int hp = 50;
	bool alive = !(hp <= 0);

	system("pause");
	return 0;
}