#include <iostream>
#include <string>
#include <cstdlib>
#include <ctime>
#include <vector>

using namespace std;

struct Roll
{
	string type;
	int rank;
	int value;
	int dice[3];
};

int wager(int money);
void payout(Roll player, Roll dealer, int bet, int& money);
void playRound(int round, int& money);
Roll rollPair();
Roll roll456();

int main()
{
	// Money should be declared here. With reference, we can pass this around.
	int money = 90000;

	for (int i = 0; i < 10; i++)
	{
		playRound(i + 1, money);
	}

	system("pause");
	return 0;
}

// We don't need to reference here. The function name 'wager' suggests that something may be returned. You should only use reference where it's necessary (not just because you can).
int wager(int money)
{
	int bet;
	cin >> bet;
	return bet;
}

Roll rollPair()
{
	Roll roll;
	roll.type = "Pair";
	roll.rank = 1;
	roll.value = 5;

	return roll;
}

Roll roll456()
{
	Roll roll;
	roll.type = "4-5-6";
	roll.rank = 2;
	roll.value = 0;

	return roll;
}

// Since 'money' is reference, we can modify it and affect the caller.
void payout(Roll player, Roll dealer, int bet, int& money)
{
	if (player.rank > dealer.rank) money += bet;
	else if (player.rank < dealer.rank) money += bet * -1;
}

// Money originally came from 'main', with referencing, we can pass values no matter how far it is from the function and still affect the source variable. 
void playRound(int round, int& money)
{
	int bet = wager(money);

	// Test roll code
	Roll player = rollPair();
	Roll dealer = roll456();

	payout(player, dealer, bet, money);
}